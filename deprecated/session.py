import redis
import os
import yaml
from sqlalchemy.orm import Session
from rich.console import Console
from sqlalchemy import create_engine
from fastapi import FastAPI, HTTPException, File, Form, UploadFile, Body, Depends, Header

import models
import schemas
from confighandler import Settings

console = Console()

if os.environ.get("MEMEDB_ENVCONFIG"):
    settings = Settings()  # Exclusively use environment variables for configuration.
elif os.path.exists(os.environ.get('CONFIG', "config.yml")):
    config = yaml.safe_load(open(os.environ.get('CONFIG', "config.yml")))
    settings = Settings.parse_obj(config)
else:
    console.print(f"No config file was found at {os.environ.get('CONFIG', 'config.yml')}, failing over to environment variables.\nIf this was intentional, set MEMEDB_ENVCONFIG=true to hide this warning.", style="bold red")
    settings = Settings()

engine = create_engine(settings.dburl)
session = Session(engine)
redis = redis.Redis(host=settings.redis.host,port=settings.redis.port)

async def ValidateSession(sessionkey: str = Body(), user: schemas.User = Depends(ActiveUser)):
    """Assert a session key is valid and its User exists and is active"""
    if redis.get(user.id) and redis.get(user.id).decode("utf-8") == sessionkey:
        return schemas.User.from_orm(user)
    else:
        raise HTTPException(status_code=401, detail="Invalid session key")