import sys
sys.path.append('../config.py')

import psycopg2
import config


def get_db_connection():
    try:
        conn = psycopg2.connect(database = config.pgDatabase, user = config.pgUser, password = config.pgPassword, host = config.pgHost, port = config.pgPort)
        return(conn)
    except:
        raise