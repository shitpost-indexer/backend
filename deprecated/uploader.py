#@app.route('/api/artifacts/upload', methods = ['POST'])
#def apiUploadArtifact():
#    if not settings.server.AllowUploads:
#        return Response('{"success": False, "msg":"File uploads are disabled in the server config!"}', status=500, mimetype='application/json')
#    else:
#        files = request.files.getlist('files')
#        print(request.files.keys())
#        fcount = 0
#        for f in files:
#            try:
#                key = request.form['sessionkey']
#                email = request.form['email']
#                fname = f.filename
#                if exists(os.path.join(settings.server.CacheDir, fname)):
#                    fname = str(random.randint(1000,5000)) + "-" + fname
#                extension = fname.split('.')[-1]
#                if not email:
#                    return Response('{"success": False, "msg":"No email provided"}', status=500, mimetype='application/json')
#                elif not key:
#                    return Response('{"success": False, "msg":"No session key provided"}', status=500, mimetype='application/json')
#                elif not f:
#                    return Response('{"success": False, "msg":"No File"}', status=500, mimetype='application/json')
#                else:
#                    if not extension in settings.server.AllowedExtensions:
#                        return Response('{"success": False, "msg":"This file type is not allowed!"}', status=500, mimetype='application/json')
#                    else:
#                        try:
#                            f.save(os.path.join(settings.server.CacheDir, fname))
#                        except:
#                            return Response('{"success": False, "msg":"Failed to save file."}', status=500, mimetype='application/json')
#                        else:
#                            if session.query(models.User).filter_by(email=email).first():
#                                try:
#                                    user = session.query(models.User).filter_by(email=email).one_or_none()
#                                    if redis.get(user.id):
#                                        if redis.get(user.id).decode("utf-8") == key:
#                                            bucket = session.query(models.Bucket).filter_by(bucketname=settings.server.DefaultInbox).one_or_none()
#                                            d = GetFileHash(fname)
#                                            hashname = d.hashname
#                                            fhash = d.hash
#                                            if session.query(models.File).filter_by(hash=fhash).first():
#                                                os.unlink(os.path.join(settings.server.CacheDir, fname))
#                                                return Response('{"success": False, "msg":"This already exists in the database"}', status=500, mimetype='application/json')
#                                                
#                                            if session.query(models.Job).filter_by(hash=fhash).first():
#                                                os.unlink(os.path.join(settings.server.CacheDir, fname))
#                                                return Response('{"success": False, "msg":"A job for this file already exists!"}', status=500, mimetype='application/json')
#                                                
#                                            else:
#                                                now = GetSQLTime()
#                                                fileid = uuid.uuid4()
#                                                jobid = uuid.uuid4()
#                                                file = models.File(
#                                                        id = fileid,
#                                                        hash = fhash,
#                                                        filename = hashname,
#                                                        creationdate = now,
#                                                        uploaduserid = user.id,
#                                                        accesscount = 0,
#                                                        bucket = bucket.id,
#                                                        processed = False
#                                                )
#                                                job = models.Job(
#                                                        id = jobid,
#                                                        hash = fhash,
#                                                        fileid = fileid,
#                                                        localfilename = fname,
#                                                        cloudfilename = hashname,
#                                                        jobtype = "import",
#                                                        creationdate = now
#                                                )
#                                                session.add(job)
#                                                session.add(file)
#                                                session.commit()
#                                                raise
#                                        else:
#                                            return Response('{"success": False, "msg":"Invalid session key"}', status=403, mimetype='application/json')  
#                                    else:
#                                        return Response('{"success": False, "msg":"Invalid session key"}', status=403, mimetype='application/json') 
#
#                                except:
#                                    if settings.server.DebugMode:
#                                        raise
#                                    else:
#                                        return Response('{"success": False, "msg":"Failed to query database"}', status=500, mimetype='application/json')  
#                            else:
#                                return Response('{"success": False, "msg":"Email does not exist"}', status=404, mimetype='application/json')
#            except:
#                return(f"Failed on {fname}")
#        return Response('{"success": True, "msg":"Upload jobs have been created!"}', status=201, mimetype='application/json')
#



#@app.route('/api/artifacts/upload', methods = ['POST'])
#def apiUploadArtifact():
#    try:
#        f = request.files['file']
#        #tagsArr = request.headers["tags"]
#        fname = f.filename
#        extension = fname.split('.')[-1]
#        if not extension in settings.server.AllowedExtensions:
#            return Response('{"success": False, "msg":"This file type is not allowed!"}', status=500, mimetype='application/json')
#        else:
#            if exists(os.path.join(settings.server.CacheDir, fname)):
#                return Response('{"success": False, "msg":"That filename is already present in the cache! (Is it waiting to be processed?)"}', status=500, mimetype='application/json')
#            f.save(os.path.join(settings.server.CacheDir, fname))
#            if not exists(os.path.join(settings.server.CacheDir, fname)):
#                return Response('{"success": False, "msg":"The file appears to have been saved, but we are not able to find it!"}', status=500, mimetype='application/json')
#            else:
#                # Get file hash
#                try:
#                    sha256_hash = hashlib.sha256()
#                    with open(os.path.join(settings.server.CacheDir, fname),"rb") as f:
#                        # Read and update hash string value in blocks of 4K
#                        for byte_block in iter(lambda: f.read(4096),b""):
#                            sha256_hash.update(byte_block)
#                        file_hash = sha256_hash.hexdigest()
#                        hashname = ''.join([file_hash,'.',str(extension)])
#                except:
#                    if settings.server.DebugMode:
#                        raise
#                    else:
#                        return Response('{"success": False, "msg":"We failed to generate a hash for the provided file."}', status=500, mimetype='application/json')
#
#                # Generate thumbnail
#                try:
#                    thumbnail = generate_thumbnail(fname)
#                except:
#                    raise
#
#                # Upload to B2
#                try:
#                    b2.Bucket(settings.b2.B2PublicBucketName).upload_file(os.path.join(settings.server.CacheDir, fname), os.path.join(settings.b2.B2RootDir, settings.b2.B2OriginDir, hashname))
#                    b2.Bucket(settings.b2.B2PublicBucketName).upload_file(os.path.join(settings.server.CacheDir, thumbnail), os.path.join(settings.b2.B2RootDir, settings.b2.B2ThumbDir, hashname))
#                    url = ''.join([settings.server.CdnUrl, '/', settings.b2.B2RootDir, '/', settings.b2.B2OriginDir, '/', hashname])
#                    turl = ''.join([settings.server.CdnUrl, '/', settings.b2.B2RootDir, '/', settings.b2.B2ThumbDir, '/', hashname])
#
#                except:
#                    os.unlink(os.path.join(settings.server.CacheDir, thumbnail))
#                    os.unlink(os.path.join(settings.server.CacheDir, fname))
#                    if settings.server.DebugMode:
#                        raise
#                    else: 
#                        return Response('{"success": False, "msg":"The file was saved to local cache, but failed to upload to B2!"}', status=500, mimetype='application/json')
#                else:
#                    # Put into DB
#                    try:
#                        InboxBucket = session.query(models.Bucket).filter_by(bucketname="inbox").first()
#                        if not InboxBucket:
#                            return Response('{"success": False, "msg":"Bucket does not exist."}', status=500, mimetype='application/json')
#                        else:
#                            now = datetime.datetime.now()
#                            sqltime = now.strftime("%Y/%m/%d %H:%M:%S")
#                            file = models.File(
#                                id = file_hash,
#                                filename = hashname,
#                                creationdate = sqltime,
#                                uploaduserid = "massive cheat lmao",
#                                accesscount = 0,
#                                bucket = "inbox"
#                            )
#                            artifacts = models.Artifact(
#                                id = file_hash,
#                                original = url,
#                                thumbnail = turl
#                            )
#
#                            tags = tagsArr.split(';')
#                            for x in tags:
#                                t = session.query(models.Tag).filter(models.Tag.tag==x).first()
#                                if not t:
#                                    tag = models.Tag(
#                                        tag_id = session.query(models.Tag).count() + 1
#                                        tag_tag = x
#                                    )
#                                    session.add(tag)
#                                    session.commit()
#                                
#                                tg = session.query(models.Tag).filter(models.Tag.tag==x).one()
#
#                                fTags = models.FileTags
#                                fTags.file_id = file.id
#                                fTags.tag_id = tg.id
#                                fTags.id = session.query(models.FileTags).count() + 1
#                                session.add(fTags)
#
#                            session.add(file)        plaintext = request.form['password'].encode()
#                    except:
#                        if settings.server.DebugMode:
#                            raise
#                        else:
#                            return Response('{"success": False, "msg":"Failed to insert into the database."}', status=500, mimetype='application/json')
#                    else:
#                        result = {
#                            "success": True,
#                            "msg": "The file has been uploaded",
#                            "artifact_origin": url,
#                            "artifact_thumb": turl
#                        }
#                        # Delete local upload
#                        os.unlink(os.path.join(settings.server.CacheDir, fname))
#                        # Delete local thumbnail
#                        os.unlink(os.path.join(settings.server.CacheDir, thumbnail))
#                        return Response(json.dumps(result), status=201, mimetype='application/json')
#    except:
#        os.unlink(os.path.join(settings.server.CacheDir,fname))
#        #os.unlink(os.path.join(settings.server.CacheDir, thumbnail))
#        if settings.server.DebugMode:
#            raise
#        else:
#            return Response('{"success": False, "msg":"An unknown error has occurred while attempting to upload file."}', status=500, mimetype='application/json')