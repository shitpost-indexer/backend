# Shitpost Indexer
# Extract URL's from a text file and make them easily searchable


import config
import os
from os.path import exists
from tqdm import tqdm
import sqlite3
import re
import time
import datetime

W = "\033[0m"  # white (normal)
R = "\033[31m"  # red
G = "\033[32m"  # green
O = "\033[33m"  # orange
B = "\033[34m"  # blue
P = "\033[35m"  # purple

print(G + "INFO: " + B + "Starting script...")

if exists(f"config.py") == False:
    print(R + "ERROR: Config file not found!")
    print(G + "INFO: " + B + "Quiting...")
    quit()
else:
    print(G + "INFO: " + B + "Config file found!")

if exists(f"{config.database}.db") == False:
    print(O + "WARN: " + B + "No database found.... creating...")
    conn = sqlite3.connect(f"{config.database}.db")
    c = conn.cursor()
    # init tabels here
    c.execute(
        "CREATE TABLE urls (`ID` BIGINT NOT NULL, `TIME` TEXT NOT NULL, `URL` TEXT NOT NULL);"
    )
    conn.commit()
    conn.close
    if exists(f"{config.database}.db") == False:
        print(R + "ERROR: Database failed to be created! Check permissions.")
        print(G + "INFO: " + B + "Quiting...")
        quit()
    else:
        print(G + "INFO: " + B + "Database created!")

else:
    print(G + "INFO: " + B + "Database found!")

conn = sqlite3.connect(f"{config.database}.db")
c = conn.cursor()

infile = input(G + "File to index: " + W)
if exists(infile) == False:
    print(R + "ERROR: File not found!")
    print(G + "INFO: " + B + "Quiting...")
    quit()
else:
    print(
        G
        + "INFO: "
        + B
        + "Starting the initial indexing process. This could take a "
        + O
        + "very long time"
    )
    print(G + "INFO: " + B + "Reading URL's from provided text file.")

    arr = []

    with open(infile) as file:
        for line in file:
            urls = re.findall(
                "(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-&?=%.]+", line
            )
            arr.append(urls)
        print(G + "INFO: " + B + "Writing database entries..." + O)
        id = 0
        for i in tqdm(arr):
            time = datetime.datetime.now().isoformat()
            c.execute(f"INSERT INTO urls VALUES ({id}, '{time}', '{i[0]}')")
            id = id + 1
        conn.commit()
        conn.close
        print(G + "INFO: " + B + "Completed!" + O)
