# SHIX
#### _The best mass-media manager you've seen!_

SHIX is A cloud-based media manager with Backblaze B2 compatibility, that offers
a wide scale of options for sorting content.


# Join the Discord server!

[![Discord](https://discordapp.com/api/guilds/974345893723648061/widget.png?style=banner4)](https://discord.gg/rk8SD7ejGr)

## Features

- Tag and sort memes for quick access, similar to your booru of choice.
- Quickly copy and paste memes to share in chat.
- Automatically compress video to specified sizes to fit within size restrictions.
- Allows sharing of content using A CDN.
- Convert to other file types Automatically

The backend was built with speed in mind, using Elasticsearch to rapidly sort large repositories of content. Alongside massive design considerations made for the UI to reduce friction in it's ease of use

## Tech

SHIX uses a number of projects to work properly:

- [Backblaze] - Backblaze B2 is an S3 compatible low cost cloud storage solution that enables us to manage terabytes of content effortlessly
- [Pydantic] - Data validation and settings management using python type annotations.
- [Postgres] - Performance relational SQL database.
- [Redis] - Redis is A high speed in memory key store.
- [Elasticsearch] - Elasticsearch is A fast search engine for searching large indexes of data.
- [Flask] - Flask is a micro web framework written in Python. 
- [Rich] - Rich is a Python library for rich text and beautiful formatting in the terminal.
- [FFMPEG] - FFmpeg is a free and open-source software project consisting of a suite of libraries and programs for handling video, audio, and other multimedia files and streams.

## Installation

We currently do not have a release candidate for public use, as everything is very much a work in progress. please check back later!


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
[Backblaze]: https://www.backblaze.com/b2/cloud-storage.html
[Postgres]: https://www.postgresql.org/
[Redis]: https://redis.io
[Elasticsearch]: https://www.elastic.co/elasticsearch/
[Flask]: https://pypi.org/project/Flask/
[Rich]: https://pypi.org/project/rich/
[FFMPEG]: https://ffmpeg.org
[Pydantic]: https://pypi.org/project/pydantic/