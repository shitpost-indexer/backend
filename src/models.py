import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy as FlaskSQLAlchemy
from flask_marshmallow import Marshmallow


metadata = db.MetaData()
Base = declarative_base(metadata=metadata)



a2t_association = db.Table('a2t_association', Base.metadata,
    db.Column('artifact_id', db.ForeignKey('artifacts.id'), primary_key=True),
    db.Column('tag_id', db.ForeignKey('tags.id'), primary_key=True)
)

class User(Base):
    __tablename__ = 'users'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, unique=False, nullable=False)
    totp = db.Column(db.Boolean, unique=False, nullable=False, server_default="0")
    totpSecret = db.Column(db.String, unique=False, nullable=True)
    pfp = db.Column(db.String, unique=False, nullable=False)
    active = db.Column(db.Boolean, unique=False, nullable=False, server_default="1")
    created = db.Column(db.DateTime, unique=False, nullable=False)
    settings = db.Column(db.JSON, unique=False, nullable=True)
    lastActivity = db.Column(db.DateTime, unique=False, nullable=True)

    favorites = db.Column(db.JSON, unique=False, nullable=True)  # needs relationships
    apikeys = relationship("APIKeys")


class APIKeys(Base):
    __tablename__ = 'apikeys'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    userid = db.Column(db.String, db.ForeignKey('users.id'))
    key = db.Column(db.String, unique=True, nullable=False)
    active = db.Column(db.Boolean, unique=False, nullable=False)

#File ID & Artifact ID & Tagset ID are the hash of the ORIGINAL imported file!!!

class Job(Base):
    __tablename__ = 'jobs'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    hash = db.Column(db.String, unique=False, nullable=False)
    fileid = db.Column(db.String, db.ForeignKey('files.id'), unique=False, nullable=False)
    localfilename = db.Column(db.String, unique=False, nullable=False)
    cloudfilename = db.Column(db.String, unique=False, nullable=False)
    jobtype = db.Column(db.String, unique=False, nullable=False)
    creationdate = db.Column(db.DateTime, unique=False, nullable=False)

    file = relationship("File")


class File(Base):
    __tablename__ = 'files'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    hash = db.Column(db.String, unique=True, nullable=False)
    filename = db.Column(db.String, unique=True, nullable=False)
    creationdate = db.Column(db.DateTime, unique=False, nullable=False)
    uploaduserid = db.Column(db.String, unique=False, nullable=False)
    accesscount = db.Column(db.Integer, unique=False, nullable=True)
    songid = db.Column(db.String, unique=False, nullable=True)
    bucket = db.Column(db.String, unique=False, nullable=False)
    tags = db.Column(db.String, unique=False, nullable=True)
    processed = db.Column(db.Boolean, unique=False, nullable=False) 

class Artifact(Base):
    __tablename__ = 'artifacts'
    id = db.Column(db.String, db.ForeignKey('files.id'), primary_key=True, unique=True, nullable=False)
    original = db.Column(db.String, unique=True, nullable=False)
    thumbnail = db.Column(db.String, unique=True, nullable=True)
    _8m = db.Column('8m', db.String, unique=True, nullable=True) # not sure about these
    _50m = db.Column('50m', db.String, unique=True, nullable=True)
    _100m = db.Column('100m', db.String, unique=True, nullable=True)
    transcoded = db.Column(db.String, unique=True, nullable=True)

    bucket_id = db.Column(db.String, db.ForeignKey('buckets.id'))

    tags = relationship("Tag", secondary=a2t_association, back_populates="artifacts")
    files = relationship("File")


class Tag(Base):
    __tablename__ = 'tags'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    name = db.Column(db.String, unique=True, nullable=False)

    artifacts = relationship(
        "Artifact",
        secondary=a2t_association,
        back_populates="tags")


class Bucket(Base):
    __tablename__ = 'buckets'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    bucketname = db.Column(db.String, unique=True, nullable=False)

    artifacts = relationship("Artifact")
