from typing import Tuple, Union
from pydantic import BaseModel, BaseSettings, PostgresDsn, SecretStr
from pydantic.env_settings import SettingsSourceCallable

class RedisSettings(BaseModel):
    host: str
    port: int
    db: int = 0

class ServerSettings(BaseModel):
    ApiName: str
    DebugMode: bool
    listenAddress: str
    listenPort: int 
    CacheDir: str
    PfpDir: str
    CdnUrl: str
    AllowedExtensions: list
    DefaultInbox: str
    DefaultArchive: str
    AllowLogin: bool
    AllowUploads: bool
    AllowAccountCreation: bool
    MaxUploadsPerRequest: str
    EnableJobRunner: bool
    JobRunnerIdleTime: int
    SQLALCHEMY_TRACK_MODIFICATIONS: Union[bool, None] = None # Deprecated, will be ignored.

class JWTSettings(BaseModel):
    SecretKey: SecretStr  # Signing key for JWTs, KEEP SECRET.
    Audience: str = "shix"  # Audience for SHIX JWTs. All JWTs will be expected to have this value. 
    Issuer: str = "shix"  # Issuer for SHIX JWTs. Primarily for audit purposes.

class BackblazeSettings(BaseModel):
    B2PublicBucketName: str
    B2EndpointURL: str
    B2KeyID: str
    B2ApplicationKey: str
    B2RootDir: str
    B2OriginDir: str
    B2ThumbDir: str

class SmartEditSettings(BaseModel):
    ThumbnailSize: int
    ThumbnailPrefix: str
    ImageExtensions: list
    
class ElasticsearchSettings(BaseModel):
    EnableElasticsearch: bool
    ElasticsearchURL: str

class IdenticonSettings(BaseModel):
    avatarSize: int
    imgSizePerCell: int



class Settings(BaseSettings):
    smartedit: SmartEditSettings
    b2: BackblazeSettings
    server: ServerSettings
    elasticsearch: ElasticsearchSettings
    redis: RedisSettings
    jwt: JWTSettings
    dburl: PostgresDsn
    identicons: IdenticonSettings


    
    class Config:
        env_nested_delimiter = '__'
        env_prefix = "memedb_"
        # Make environment variables take precendence over the config file.
        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> Tuple[SettingsSourceCallable, ...]:
            return env_settings, init_settings, file_secret_settings
