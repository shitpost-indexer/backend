#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~IMPORTS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import uvicorn


from rich.traceback import install

install(show_locals=False)

from rich.console import Console

console = Console()

import atexit
import datetime
import hashlib
import json
import os
import random
import string
import threading
import time
import uuid
from multiprocessing import Process
from os.path import exists

import boto3
import nacl.pwhash
import pyotp
import python_avatars as pa
import redis
import yaml
from botocore.config import Config
from botocore.exceptions import ClientError
from elasticsearch import Elasticsearch
from passlib.context import CryptContext
from PIL import Image
from rich.markdown import Markdown
from rich.pretty import pprint
from rich.text import Text
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

import models
import schemas
import utils.tests as tests
import utils.utils as utils
from confighandler import Settings
from fastapi import FastAPI, HTTPException, File, Form, UploadFile, Body, Depends, Header
import jwt as pyjwt

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~END OF IMPORTS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Get start time of script
ServerStartTime = time.time()

if os.environ.get("MEMEDB_ENVCONFIG"):
    settings = Settings()  # Exclusively use environment variables for configuration.
elif os.path.exists(os.environ.get('CONFIG', "config.yml")):
    config = yaml.safe_load(open(os.environ.get('CONFIG', "config.yml")))
    settings = Settings.parse_obj(config)
else:
    console.print(f"No config file was found at {os.environ.get('CONFIG', 'config.yml')}, failing over to environment variables.\nIf this was intentional, set MEMEDB_ENVCONFIG=true to hide this warning.", style="bold red")
    settings = Settings()



versionNumber = open("VERSION.TXT", "r").read().replace('\n','')
versionInfo = {
    "name": settings.server.ApiName,
    "version": versionNumber
}



# Setup Redis Connection
redis = redis.Redis(host=settings.redis.host,port=settings.redis.port)


#Setup B2
def GetB2Conn():
    b2 = boto3.resource(
            service_name='s3',
            endpoint_url=settings.b2.B2EndpointURL,     # Backblaze endpoint
            aws_access_key_id=settings.b2.B2KeyID,  # Backblaze keyID
            aws_secret_access_key=settings.b2.B2ApplicationKey,  # Backblaze applicationKey
            config=Config(
                signature_version='s3v4',
            ))

    return(b2)

b2 = GetB2Conn()

#Get Elastisearch
if settings.elasticsearch.EnableElasticsearch:
    es = Elasticsearch(settings.elasticsearch.ElasticsearchURL)



# Flask
# app = Flask(__name__)
# app.debug = False
# app.use_reloader = False

# FastAPI
app = FastAPI()


# Initialize sqlalchemy + marshmallow
engine = create_engine(settings.dburl)
session = Session(engine)
md = models.metadata
md.bind = engine

#models.ma.init_app(app)

# Setup threading for fastapi
# TODO: NUKE: not used for fastapi
# ^ This is a lie. its now used for fastapi.
app_thread = threading.Thread(target=lambda: uvicorn.run("server:app", host=settings.server.listenAddress, port=settings.server.listenPort, log_level="info"))
app_thread.daemon = True


async def GetUser(email: str = Body()):
    """Assert a User exists by their email"""
    user = session.query(models.User).filter_by(email=email).one_or_none()
    if user:
        return user
    else:
        raise HTTPException(status_code=400, detail="User does not exist")

async def ActiveUser(user: schemas.User = Depends(GetUser)):
    """Assert a User exists and is active by their email"""
    if user.active:
        return schemas.User.from_orm(user)
    else:
        raise HTTPException(status_code=400, detail="Account is disabled")

async def ValidateJWT(authorization: str = Header()):
    """Authenticate a user by a JWT in the Authorization header"""

    if authorization.startswith("JWT"):
        jwt = authorization.replace("JWT ", "")
        try:
            jwt = pyjwt.decode(
                jwt,
                settings.jwt.SecretKey.get_secret_value(),
                algorithms=["HS256"],
                # Require basic claims and disable certain checks because we'll be doing them on our own in UTC.
                options={"require": ["iss", "aud", "sub", "exp", "nbf", "iat"], "verify_exp": False, "verify_nbf": False},
                audience=[settings.jwt.Audience],
            )
        except pyjwt.PyJWTError as err:
            raise HTTPException(status_code=401, detail="Token is invalid")

        if datetime.datetime.fromtimestamp(jwt['nbf']) > datetime.datetime.utcnow():
            raise HTTPException(status_code=401, detail="The token is not yet valid")
        elif datetime.datetime.fromtimestamp(jwt['exp']) < datetime.datetime.utcnow():
            raise HTTPException(status_code=401, detail="The token has expired")
        elif not isinstance(jwt['iat'], float) and not isinstance(jwt['iat'], int):
            raise HTTPException(status_code=401, detail="The token's issued time is invalid")

        account = session.query(models.User).where(models.User.id == jwt["sub"]).one_or_none()
        if not account:
            raise HTTPException(status_code=401, detail="The token's user does not exist.")
        elif not account.active:
            raise HTTPException(status_code=400, detail="Account is disabled")
        else:
            return schemas.User.from_orm(account)
    else:
        raise HTTPException(status_code=400, detail="Unknown authorization method")

def JobRunner():
    try:
        console.print("Starting job runner!", style="bold green")
        while True:
            time.sleep(settings.server.JobRunnerIdleTime)
            if session.query(models.Job).filter_by(jobtype="import").first():
                count = session.query(models.Job).filter_by(jobtype="import").count()
                console.print(f"Found {count} new import jobs!", style="bold yellow")
                totaltimestart = time.time()
                for job in session.query(models.Job).filter_by(jobtype="import").all():
                    jobtimestart = time.time()
                    fname = job.localfilename
                    jobid = job.id
                    extension = fname.split('.')[-1]
                    if not extension in settings.smartedit.ImageExtensions:
                        pass
                    else:
                        try:
                            thumbnail = utils.generate_thumbnail(settings, fname)
                            b2.Bucket(settings.b2.B2PublicBucketName).upload_file(os.path.join(settings.server.CacheDir, fname), os.path.join(settings.b2.B2RootDir, settings.b2.B2OriginDir, job.cloudfilename))
                            b2.Bucket(settings.b2.B2PublicBucketName).upload_file(os.path.join(settings.server.CacheDir, thumbnail), os.path.join(settings.b2.B2RootDir, settings.b2.B2ThumbDir, job.cloudfilename))
                            url = ''.join([settings.server.CdnUrl, '/', settings.b2.B2RootDir, '/', settings.b2.B2OriginDir, '/', job.cloudfilename])
                            turl = ''.join([settings.server.CdnUrl, '/', settings.b2.B2RootDir, '/', settings.b2.B2ThumbDir, '/', job.cloudfilename])
                        except:
                            raise
                            #console.log(f"Import job failed! Removing.", style="bold red")
                            session.delete(job)
                            session.commit()
                        else:
                            os.unlink(os.path.join(settings.server.CacheDir, fname))
                            os.unlink(os.path.join(settings.server.CacheDir, thumbnail))
                            artifacts = models.Artifact(
                                            id = job.fileid,
                                            original = url,
                                            thumbnail = turl
                                        )
                            session.add(artifacts)
                            session.delete(job)
                            session.commit()
                            jobtimeend = time.time()
                            jobtime = jobtimeend - jobtimestart
                            console.print(f"Finished job: {jobid} in {jobtime}", style="bold green")    
                totaltimeend = time.time()
                totaltime = totaltimeend - totaltimestart
                console.print(f"Finished all {count} jobs! Took {totaltime}", style="bold green")
    except KeyboardInterrupt:
        console.print("Stopping runner.", style="bold yellow") 
                
                
# Initialize Import Job Runner
JobRunnerProcess = Process(target=JobRunner)
JobRunnerProcess.daemon = True


# Flask API Endpoints
@app.get('/')
def landing():
   return versionInfo

@app.post(
    '/api/artifacts/upload', 
    status_code=201, 
    name="Artifact Upload", 
    description="Upload an file as an artifact to SHIX.\n\nThis is the only endpoint which uses a multipart-form.",
    tags=['Artifact']
    )
async def apiUploadArtifact(files: list[UploadFile], account: schemas.User = Depends(ValidateJWT)):
    if not settings.server.AllowUploads:
        raise HTTPException(status_code=403, detail="File uploads are disabled in the server config!")
    else:
        bucket = session.query(models.Bucket).filter_by(bucketname=settings.server.DefaultInbox).one_or_none()
        status = {}   
        uploads = []

        for file in files:
            try:
                fname = file.filename
                if exists(os.path.join(settings.server.CacheDir, fname)):
                    fname = str(random.randint(1000,5000)) + "-" + fname
                extension = fname.split('.')[-1]
                if not extension in settings.server.AllowedExtensions:
                    status[fname] = "This file type is not allowed!"
                else:
                    # SpooledTemporaryFile doesn't have .save() so this will do about the same thing
                    with open(os.path.join(settings.server.CacheDir, fname), mode='bw') as f:
                        f.write(file.file.read())
                    uploads.append(fname)
            except:
                status[fname] = "Failed to upload file"

    
        for upload in uploads:
            hasher = utils.GetFileHash(settings, upload)
            hashn = hasher.hashname
            hashf = hasher.hash
            if session.query(models.File).filter_by(hash=hasher.hash).first():
                os.unlink(os.path.join(settings.server.CacheDir, upload))
                status[upload] = "File already exists in the database!"
            elif session.query(models.Job).filter_by(hash=hasher.hash).first():
                os.unlink(os.path.join(settings.server.CacheDir, upload))
                status[upload] = "Job already exists in the database!"
            else:
                try:
                    now = utils.GetSQLTime()
                    fileid = uuid.uuid4()
                    jobid = uuid.uuid4()
                    file = models.File(
                            id = fileid,
                            hash = hashf,
                            filename = hashn,
                            creationdate = now,
                            uploaduserid = account.id,
                            accesscount = 0,
                            bucket = bucket.id,
                            processed = False
                    )
                    
                    esindex = {
                        'id': fileid,
                        'hash': hashf,
                        'filename': hashn,
                        'creationdate': now,
                        'uploaduserid': account.id,
                        'accesscount': 0,
                        'bucket': bucket.id,
                        'processed': False        
                    }
                    
                    job = models.Job(
                            id = jobid,
                            hash = hashf,
                            fileid = fileid,
                            localfilename = upload,
                            cloudfilename = hashn,
                            jobtype = "import",
                            creationdate = now
                    )
                    
                    if settings.elasticsearch.EnableElasticsearch:
                        resp = es.index(index="shix-files", id=fileid, document=esindex)
                    session.add(job)
                    session.add(file)
                    session.commit()
                    status[upload] = f"Job has been created!"
                except:
                    raise
    
    status["Total Uploads"] = len(uploads)
    return status


@app.post(
    '/api/accounts/create', 
    status_code=201,
    name="Create Account",
    description="Create an account on SHIX.",
    tags=['User']
    )
def apiCreateAccount(user: schemas.APIUserCreate):
    if not settings.server.AllowAccountCreation:
        raise HTTPException(status_code=403, detail="Account Creation is disabled.")
    else:
        username = user.username
        phash = utils.GetPasswordHash(user.password)
        email = user.email

        try:
            if session.query(models.User).filter_by(username=username).first():
                raise HTTPException(status_code=400, detail="Username is already taken")
        except:
            if settings.server.DebugMode:
                raise
            else:
                raise HTTPException(status_code=500, detail="Failed to query database")
        else:
            try:            
                if session.query(models.User).filter_by(email=user.email).first():
                    raise HTTPException(status_code=400, detail="Email is already in use")
            except:
                raise HTTPException(status_code=500, detail="Failed to query database")
            else:
                try:
                    avatar = utils.GenerateIdenticon(settings, username)
                    if not avatar:
                        if settings.server.DebugMode:
                            raise
                        else:
                            raise HTTPException(status_code=500, detail="Failed to generate random pfp")

                except:
                    if settings.server.DebugMode:
                        raise
                    else:
                        raise HTTPException(status_code=500, detail="Failed to generate random pfp")
                else:
                    try:
                        b2.Bucket(settings.b2.B2PublicBucketName).upload_file(avatar, os.path.join(settings.b2.B2RootDir, settings.server.PfpDir, username) + ".png")
                        avatarurl = ''.join([settings.server.CdnUrl, '/', settings.b2.B2RootDir, '/', settings.server.PfpDir, '/', username, '.png'])
                        os.unlink(os.path.join(settings.server.CacheDir, username) + ".png")

                    except:
                        if settings.server.DebugMode:
                            raise
                        else:
                            raise HTTPException(status_code=500, detail="Failed to upload pfp")
                    else:
                        try:
                            now = utils.GetSQLTime()
                            account = models.User(
                                            id = uuid.uuid4(),
                                            username = username,
                                            email = email,
                                            password = phash,
                                            pfp = avatarurl,
                                            active = True,
                                            created = now,
                                            lastActivity = now
                                        )
                            session.add(account)
                            session.commit()
                        except:
                            if settings.server.DebugMode:
                                raise
                            else:
                                raise HTTPException(status_code=500, detail="Failed to write to database")
                        else:
                            result = {
                                "success": True,
                                "msg": "Account has been created",
                                "username": username,
                                "email": email,
                                "pfp": avatarurl
                            }
                            return result


@app.post(
    '/api/accounts/login',
    name="User Login",
    description="Log into a SHIX account and receive a session key to perform actions with.",
    tags=['User']
    )
def apiLoginAccount(password: str = Body(), totpcode: str = Body(default=None), account: schemas.User = Depends(ActiveUser)):
    if not settings.server.AllowLogin:
        raise HTTPException(status_code=403, detail="Account Login is disable in the servers config.")
    else:
        if utils.VerifyPasswordHash(password, account.password.get_secret_value()):
            if account.totp:
                if not totpcode:
                    # Emulate a missing field.
                    detail=[{"loc": ["body", "totpcode"], "msg": "totp 2fa enabled, field required","type": "value_error.missing"}]
                    raise HTTPException(status_code=401, detail=detail)
                elif not pyotp.TOTP(account.totpSecret.get_secret_value()).verify(totpcode):
                    raise HTTPException(status_code=401, detail="TOTP code was invalid.")

            sqlaccount = session.query(models.User).filter_by(email=account.email).one_or_none()
            sqlaccount.lastActivity = utils.GetSQLTime()
            session.add(sqlaccount)
            session.commit()

            jwt = pyjwt.encode(
                {
                    "iss": settings.jwt.Issuer,
                    "sub": account.id,
                    "aud": settings.jwt.Audience,
                    "exp": (datetime.datetime.utcnow() + datetime.timedelta(days=1)).timestamp(),
                    "nbf": datetime.datetime.utcnow().timestamp(),
                    "iat": datetime.datetime.utcnow().timestamp()
                },
                settings.jwt.SecretKey.get_secret_value(),
                algorithm="HS256"
            )
            result = {
                "success": True,
                "msg": "Logged into account!",
                "pfp": account.pfp,
                "username": account.username,
                "sessionkey": "JWT " + jwt
            }
            return result
        else:
            raise HTTPException(status_code=401, detail="Invalid email or password")

@app.post(
    '/api/accounts/totp/setup',
    name="Setup Two-Factor Authentication",
    description="Generate a TOTP Two-Factor Authentication secret for an account.",
    tags=['User']
    )
def apiSetupTotpAccount(password: str = Body(), account: schemas.User = Depends(ValidateJWT)):
    user = session.query(models.User).filter_by(email=account.email).one_or_none()

    if utils.VerifyPasswordHash(password, user.password):
        if not user.totp:
            token = pyotp.random_base32()
            user.totpSecret = token
            session.add(user)
            session.commit()

            result = {
                "sucess": True,
                "msg": "TOTP Setup.",
                "token": token
            }
            return result
        else:
            raise HTTPException(status_code=400, detail="TOTP already enabled")
    else:
        raise HTTPException(status_code=401, detail="Invalid password")

@app.post(
    '/api/accounts/totp/enable',
    name="Enable Two-Factor Authentication",
    description="Enable TOTP Two-Factor Authentication with confirmation via generated code.",
    tags=['User']
    )
def apiEnableTotpAccount(otp: str = Body(), account: schemas.User = Depends(ValidateJWT)):
    user = session.query(models.User).filter_by(email=account.email).one_or_none()
    if not user.totp:
        itotp = pyotp.TOTP(user.totpSecret)
        if itotp.verify(otp):
            user.totp = True
            session.add(user)
            session.commit()
            return {"success": True, "msg":"TFA Enabled"}
        else:
            raise HTTPException(status_code=401, detail="Invalid TOTP code provided")
    else:
        raise HTTPException(status_code=400, detail="TOTP already enabled")


@app.post(
    '/api/accounts/logout',
    name="User Logout",
    description="Log out of an account and invalidate the session key.",
    tags=['User']
    )
def apiLogoutAccount(account: schemas.User = Depends(ValidateJWT)):
    redis.delete(account.id)
    return {"success": True, "msg":"User logged out"}


@app.get(
    '/api/status',
    name="SHIX Status",
    description="Return API information such as health status, version and uptime, and server configuration.",
    tags=['Status']
    )
async def apiStatus():
    UptimeInSeconds = time.time() - ServerStartTime
    CleanUptime = utils.GetNiceTime(UptimeInSeconds)
    GlobalConfig = {
    "CdnUrl": settings.server.CdnUrl,
    "AllowedExtensions": settings.server.AllowedExtensions,
    "DefaultInbox": settings.server.DefaultInbox,
    "DefaultArchive": settings.server.DefaultArchive,
    "AllowAccountCreation": settings.server.AllowAccountCreation,
    "AllowUploads": settings.server.AllowUploads,
    "AllowLogin": settings.server.AllowLogin,
    "EnableJobRunner": settings.server.EnableJobRunner,
    "MaxUploadsPerRequest": settings.server.MaxUploadsPerRequest
    }
    try:
        if tests.redisTest(redis) == True:
            return {'success': True, 'detail': "Health check success", "uptime": CleanUptime, "info": versionInfo, "config": GlobalConfig}
    except:
        raise HTTPException(status_code=500, detail="Health check failure")

@app.post(
    '/api/buckets/add', 
    status_code=201,
    name="Create Bucket",
    description="Create a bucket for Artifacts",
    tags=['Bucket']
    )
def apiAddBucket(bucket: schemas.APIBucket):
    try:
        import uuid
        name = str(bucket.name)
        if session.query(models.Bucket).filter_by(bucketname=name).first():
            raise HTTPException(status_code=400, detail="Bucket already exists")
        else:
            bucket = models.Bucket(
                id = str(uuid.uuid4()),
                bucketname = name
            )
            dump = schemas.Bucket.from_orm(bucket)  # TODO: serverside uuid4 generation

            session.add(bucket)
            session.commit()
    except:
        return HTTPException(status_code=500)
    else:
        return dump

@app.get(
    '/api/buckets/get',
    name="List buckets",
    description="Get a list of all available buckets.",
    tags=['Bucket']
    )
def apiGetBucket():
    try:
        bucket = session.query(models.Bucket).all()
        buckets = list()
        for b in bucket:
            buckets.append(schemas.Bucket.from_orm(b).dict())

        return buckets
    except:
        return HTTPException(status_code=500)

 
# Main, as the name states is where your stuff should be done, outside any work done on startup. that should be done in init


# Anything in exit_handler is ran on program exit.
def exit_handler():
    console.print('Shutting down process.', style="bold yellow")
    JobRunnerProcess.terminate()
    #db.close()


def main():
    while True:
        pass


# tasks to perform on start
def setup():
    console.print("Starting setup!", style="bold yellow")

    # Create Tables
    try:
        md.create_all()
    except:
        raise Exception("Failed to create tables")
    else:
        console.print("Created tables!", style="bold green")

    # Create default buckets
    default_bucket1 = models.Bucket(
        id = str(uuid.uuid4()),
        bucketname = settings.server.DefaultInbox
    )
    default_bucket2 = models.Bucket(
        id = str(uuid.uuid4()),
        bucketname = settings.server.DefaultArchive
    )
    if not session.query(models.Bucket).filter_by(bucketname=settings.server.DefaultInbox).first():
        session.add(default_bucket1)
        console.print(f'Created bucket named {settings.server.DefaultInbox}', style="bold yellow")
    if not session.query(models.Bucket).filter_by(bucketname=settings.server.DefaultArchive).first():
        session.add(default_bucket2)
        console.print(f'Created bucket named {settings.server.DefaultArchive}', style="bold yellow")

    session.commit()

    # Create Cache Directory
    try:
        if not os.path.exists(settings.server.CacheDir):
            os.mkdir(settings.server.CacheDir)
            console.print("Created Cache Dir", style="bold yellow")
    except:
        raise

    # Make lock file
    with open("SETUP_LOCK", 'w') as fp:
        fp.write("Setup Locked")
    
    #Return to init
    console.print("Setup finished!", style="bold green")
    init()



def init():
    # Check for setup lock
    if not exists("SETUP_LOCK"):
        console.print("First time setup. creating tables", style="bold yellow")
        try:
            setup()
        except:
            raise Exception("Failed to run setup!")

    # Run the redis test in util/tests.py
    #if True:
    if tests.redisTest(redis) == True:
        console.print("Passed Redis", style="bold green")
    else:
        raise Exception("Redis test returned False")
    
    # Start FastAPI
    app_thread.start()
    
    # Start Import Job Runner
    if settings.server.EnableJobRunner:
        JobRunnerProcess.start()

    main()


if __name__ == "__main__":
    try:
        init()
    except KeyboardInterrupt:
        console.print("You have requested a shutdown.", style="bold yellow")
    except:
        console.print_exception(show_locals=True)


atexit.register(exit_handler)
