from __future__ import annotations
from typing import Optional, List, Dict
import uuid, json
from uuid import UUID

from pydantic import BaseModel, Field, Json, SecretStr
from datetime import datetime

class User(BaseModel):
    class Config:
        orm_mode = True

    id: str
    username: str
    email: str
    password: SecretStr
    totp: bool = False
    totpSecret: SecretStr = None
    pfp: str
    active: bool
    created: datetime
    # settings: Optional[Json]
    # favorites: Optional[Json]
    lastActivity: datetime

class APIKey(BaseModel):
    class Config:
        orm_mode = True

    id: str
    userid: str
    key: str
    active: bool

class Job(BaseModel):
    class Config:
        orm_mode = True

    id: str
    hash: str
    fileid: str
    localfilename: str
    cloudfilename: str
    jobtype: str
    creationdate: datetime

class File(BaseModel):
    class Config:
        orm_mode = True

    id: str
    hash: str
    filename: str
    creationdate: datetime
    uploaduserid: str
    accesscount: int
    songid: str
    bucket: str
    tags: str
    processed: bool

class Artifact(BaseModel):
    class Config:
        orm_mode = True

    id: str
    original: str
    thumbnail: str
    _8m: str
    _50m: str
    _100m: str
    transcoded: str

    bucket_id: str

class Tag(BaseModel):
    class Config:
        orm_mode = True

    id: str
    name: str

class Bucket(BaseModel):
    class Config:
        orm_mode = True

    id: str
    bucketname: str


# API Models


class APIUserCreate(BaseModel):
    username: str
    email: str
    password: str

class APIBucket(BaseModel):
    name: str