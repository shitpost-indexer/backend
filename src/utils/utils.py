import os
from PIL import Image
import hashlib
from passlib.context import CryptContext
import datetime
import utils.identicon as identicon
import time

# Generate thumbnail
def generate_thumbnail(settings, file_name):
    imgextensions = settings.smartedit.ImageExtensions
    extension = file_name.split('.')[-1]
    if extension in imgextensions:
        try:
            image = Image.open(os.path.join(settings.server.CacheDir, file_name))
            aspect_ratio = image.height / image.width
            new_height = settings.smartedit.ThumbnailSize * aspect_ratio
            thumbnail = image.resize((settings.smartedit.ThumbnailSize, int(new_height)), Image.Resampling.LANCZOS)
            thumbname = settings.smartedit.ThumbnailPrefix + file_name
            thumbnail.save(os.path.join(settings.server.CacheDir, thumbname))
            return(os.path.join(thumbname))
        except:
            raise\
                
                
# Get File Hash
class GetFileHash:
    def __init__(self, settings, fname):
        extension = fname.split('.')[-1]
        sha256_hash = hashlib.sha256()
        with open(os.path.join(settings.server.CacheDir, fname),"rb") as f:
                # Read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(1048576),b""):
                sha256_hash.update(byte_block)
                file_hash = sha256_hash.hexdigest()
                self.hash = file_hash
                self.hashname = ''.join([file_hash,'.',str(extension)])


pwd_context = CryptContext(
    # Replace this list with the hash(es) you wish to support.
    # this example sets pbkdf2_sha256 as the default,
    # with additional support for reading legacy des_crypt hashes.
    schemes = ["argon2"],

    # Automatically mark all but first hasher in list as deprecated.
    # (this will be the default in Passlib 2.0)
    deprecated="auto"
)    

# Get password hash
def GetPasswordHash(plaintext):
        phash = pwd_context.hash(plaintext)

        return phash

# Verify Password Hash
def VerifyPasswordHash(plaintext, phash):
        if pwd_context.verify(plaintext, phash):
            return True
        else:
            return False
        
# Get SQL time
def GetSQLTime():
    now = datetime.datetime.now()
    sqltime = now.strftime("%Y/%m/%d %H:%M:%S")
    return(sqltime)

def GenerateIdenticon(settings, username):
    identicon.create_identicon(username, os.path.join(settings.server.CacheDir, username) + ".png", settings.identicons.avatarSize, settings.identicons.imgSizePerCell)
    if os.path.exists(os.path.join(settings.server.CacheDir, username) + ".png"):
        return(os.path.join(settings.server.CacheDir, username) + ".png")
    else:
        return(False)

def GetNiceTime(seconds):
    min, sec = divmod(seconds, 60)
    hour, min = divmod(min, 60)
    day, hour = divmod(hour, 24)
    return "%04d:%02d:%02d:%02d" % (day, hour, min, sec)