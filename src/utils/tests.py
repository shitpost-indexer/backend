import random

def redisTest(redisConn):
    try:
        randomi = random.randint(0,10000)
        redisConn.set('test',randomi)
        result = redisConn.get('test')
        if int(result) == randomi:
            return(True)
        else:
            return(Exception("Redis test returned incorrect value"))
    except:
        raise